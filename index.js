/*Créez une fonction qui prend un nombre comme argument. 
Additionnez tous les nombres de 1 au nombre que vous avez passé à
 la fonction. Par exemple, si l'entrée est 4, 
votre fonction doit renvoyer 10 car 1 + 2 + 3 + 4 = 10.*/

function addUp(num) {
	let count = 0
	for(let i=1; i<=num; i++){
		count += i;
	}
	return count;
}
/*Écrivez une fonction qui convertit hoursen secondes*/
function howManySeconds(hours) {
	
	return hours*60*60
}
/*Créez une fonction qui prend un nombre comme argument, incrémente le nombre de +1
 et renvoie le résultat.*/
 function addition(num) {
	return num + 1
}
/*Créez une fonction qui prend l'âge en années et renvoie l'âge en jours.*/
function calcAge(age) {
	return age* 365;
}
/*Écrivez une fonction qui renvoie la chaîne "something"jointe
 par un espace " "et l'argument donné a.*/
 function giveMeSomething(a) {
	return "something "+a;
}
/*Corrigez le code dans l'onglet code pour réussir ce défi (uniquement les erreurs
     de syntaxe). Regardez les exemples ci-dessous pour avoir une idée de ce que la fonction doit faire.
*/
function cubes(a) {
	return a ** 3
}
Créez une fonction qui prend un nombre (étape) comme argument et renvoie le
nombre d'allumettes dans cette étape. Voir les étapes 1, 2 et 3 dans l'image 
ci-dessus.

